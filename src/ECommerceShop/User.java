package ECommerceShop;

public class User {

	public String firstname;
	public String lastname;
	public String email;
	public String password;
	

	public User(String firstname, String lastname, String email, String password) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
	}
	
	public String getFullName() {
		return this.firstname + " " + this.lastname; 
	}
}