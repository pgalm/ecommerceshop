package ECommerceShop;

import java.util.Objects;
import java.util.ArrayList;

public class UsersManagement {

	private static ArrayList<User> users = new ArrayList<User>();

	public static User loggedInUser;

	public static ArrayList<User> getUsers() {
		return users;
	}

	public static String createUser(String firstName, String lastName, String password, String email) {
		for (User u : users) {
			if (Objects.equals(u.email, email)) {
				return "!!!This email is already used!!!";
			}
		}
		User newUser = new User(firstName, lastName, email, password);
		users.add(newUser);
		return "New user is created";
	}

	public static Boolean signUp() {
		System.out.println("*** Sign Up ***");
		System.out.println("Please enter your first name: ");
		String firstName = MainMenuClass.readNextString();
		if (firstName == "") {
			System.out.println("!!!Please enter your first name!!!");
			return true;
		}
		System.out.println("Please enter your last name: ");
		String lastName = MainMenuClass.readNextString();
		if (lastName == "") {
			System.out.println("!!!Please enter your last name!!!");
			return true;
		}
		System.out.println("Please enter your password: ");
		String password = MainMenuClass.readNextString();
		if (password == "") {
			System.out.println("!!!Please enter your password!!!");
			return true;
		}
		System.out.println("Please enter your email: ");
		String email = MainMenuClass.readNextString();
		if (email == "") {
			System.out.println("!!!Please enter your email!!!");
			return true;
		}

		System.out.println(UsersManagement.createUser(firstName, lastName, password, email));
		return false;
	}

	public static Boolean signIn() {
		UsersManagement.createUser("paul", "galm", "admin123", "admin");

		if (loggedInUser != null) {
			System.out.println("");
			System.out.println("*** Sign Out ***");
			loggoutUser();
			return false;
		}

		System.out.println("");
		System.out.println("*** Sign In ***");
		System.out.println("Please enter your email: ");
		String email = MainMenuClass.readNextString();
		System.out.println("Please enter your password: ");
		String password = MainMenuClass.readNextString();

		for (User u : users) {
			if (Objects.equals(u.password, password) && Objects.equals(u.email, email)) {
				System.out.println("Glad to see you back " + u.getFullName());
				loggedInUser = u;
				return true;
			}
		}
		System.out.println("Unfortunately, such login and password doesn�t exist");
		return false;
	}

	public static boolean isUserLoggedIn() {
		return loggedInUser != null;
	}

	public static String changeUserPassword(String email, String password) {
		for (Integer z = 0; z < users.size(); ++z) {
			if (Objects.equals(email, users.get(z).email)) {
				User changeUser = users.get(z);
				changeUser.password = password;
				users.set(z, changeUser);
				return "Password has been succesfully changed";
			}
		}
		return "!!!Something failed while changing the password!!!";
	}

	public static String changeUserEmail(String emailOld, String emailNew) {
		for (Integer z = 0; z < users.size(); ++z) {
			if (Objects.equals(emailOld, users.get(z).email)) {
				User changeUser = users.get(z);
				changeUser.email = emailNew;
				users.set(z, changeUser);
				return "Email has been succesfully changed";
			}
		}
		return "!!!Something failed while changing the email!!!";
	}

	public static void showCustomerList() {
		System.out.println("");
		System.out.println("*** Customer List ***");
		for (User u : users) {
			System.out.println("Name: " + u.getFullName() + " | Email: " + u.email);
		}
	}

	private static void loggoutUser() {
		loggedInUser = null;
		ProductManagement.checkoutPossible = false;
		System.out.println("Have a nice day! Look forward to welcoming back!");
	}
}
