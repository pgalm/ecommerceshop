package ECommerceShop;

import java.util.Scanner;

public class MainMenuClass {

	public static void main(String[] args) {
		while (true) {
			showMenu();
		}
	}
	
	private static Scanner myScanner = new Scanner(System.in);
	
	public static Integer readNextInteger() {
		return Integer.parseInt(myScanner.nextLine());
	}
	
	public static String readNextString() {
		return myScanner.nextLine();
	}

	private static void showMenu() {
		System.out.println("");
		System.out.println("**** Main Menu ****");
		System.out.println("Please, enter number in console to proceed.");
		System.out.println("1. Sign Up");
		if (UsersManagement.isUserLoggedIn()) {
			System.out.println("2. Sign Out");
		} else {
			System.out.println("2. Sign In");
		}
		System.out.println("3. Product Catalog");
		System.out.println("4. My Orders");
		System.out.println("5. Settings");
		System.out.println("6. Customer List");

		Integer userInput = readNextInteger();
		if (!(userInput <= 6) && !(userInput >= 1)) {
			System.out.println("!!! Please insert a number between 1 and 6 !!!");
			System.out.println("");
			return;
		}
		switch (userInput) {
		case 1:
			System.out.println("");
			while (UsersManagement.signUp()) {}
			break;
		case 2:
			System.out.println("");
			UsersManagement.signIn();
			break;
		case 3:
			System.out.println("");
			while (ProductManagement.handleProducts()) {}
			break;
		case 4:
			System.out.println("");
			OrdersManagement.showOrders();
			break;
		case 5:
			System.out.println("");
			Settings.showSettings();
			break;
		case 6:
			System.out.println("");
			UsersManagement.showCustomerList();
			break;
		}
	}

}
