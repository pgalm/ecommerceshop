package ECommerceShop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class CartManagement {

	public static String createCartEntry(String email, Integer productid) {
		ArrayList<CartEntry> cartList = getUserCart(email);
		for (CartEntry c : cartList) {
			if (c.productid == productid) {
				return "!!!This product is already in your cart!!!";
			}
		}
		cartList.add(new CartEntry(productid));
		ProductManagement.checkoutPossible = true;
		return "Product has been added to your cart.";
	}
	
	public static HashMap<String, ArrayList<CartEntry>> userCarts = new HashMap<>();
	
	public static ArrayList<CartEntry> getUserCart(String email) {
		if(!userCarts.containsKey(email)) {
			userCarts.put(email, new ArrayList<CartEntry>());
		}
		return userCarts.get(email);
	}
	
	public static void showUserCart() {
		System.out.println("");
		System.out.println("*** Cart ***");
		ArrayList<CartEntry> cartList = getUserCart(UsersManagement.loggedInUser.email);
		for (CartEntry c : cartList) {
			for(Product p : ProductManagement.getProducts()) {
				if(Objects.equals(c.productid, p.id)) {
					System.out.println("Id: " + p.id + " | Name: " + p.productName + " | Category: " + p.categoryName
							+ " | Price: " + p.price);
				}
			}
		}
		System.out.println("");
		System.out.println("*** Product Catalog ***");
	}
}
