package ECommerceShop;

import java.util.ArrayList;
import java.util.Objects;

public class ProductManagement {

	private static ArrayList<Product> productList = new ArrayList<Product>();
	public static Boolean checkoutPossible = false;
	public static Integer productCreatingCounter = 0;

	public static ArrayList<Product> getProducts() {
		return productList;
	}

	private static void createProducts() {
		productList.add(new Product(1, "Harry Potter Collection", "Movies", 29.99));
		productList.add(new Product(2, "Fast and Furious Collection", "Movies", 49.99));
		productList.add(new Product(3, "Garden Chair", "Furntiture", 109.99));
		productList.add(new Product(4, "Couch", "Furntiture", 259.99));
		productList.add(new Product(5, "Java ist auch eine Insel", "Book", 29.99));
		productList.add(new Product(6, "Amazon Fire TV Stick 4K", "IOT", 49.99));
		productList.add(new Product(7, "Razer Kraken X", "Headset", 28.91));
		productList.add(new Product(8, "Razer BlackShark V2 X", "Headset", 29.99));
		productList.add(new Product(9, "Tesla Model Y Performance", "Car", 63_990.00));
		productList.add(new Product(10, "Tesla Model 3 Performance", "Car", 55_990.00));
	}

	public static Boolean handleProducts() {
		System.out.println("");
		System.out.println("*** Product Catalog ***");
		if (productCreatingCounter == 0) {
			createProducts();
			++productCreatingCounter;
		}

		System.out.println(
				"Enter product id to add it to the cart or �menu� if you want to navigate back to the main menu");
		
		for (Product p : productList) {
			System.out.println("Id: " + p.id + " | Name: " + p.productName + " | Category: " + p.categoryName
					+ " | Price: " + p.price);
		}
		if (checkoutPossible) {
			System.out.println("Type �checkout� if you want to checkout or �cart� if you want to see your cart");
		}
		String userInput = MainMenuClass.readNextString();
		if (checkoutPossible) {
			if (Objects.equals(userInput, "checkout")) {
				while (checkout()) {
				}
				return false;
			}else if (Objects.equals(userInput, "cart")) {
				CartManagement.showUserCart();
				return true;
			}
		}
		if (Objects.equals(userInput, "menu")) {
			return false;
		} else if (UsersManagement.loggedInUser == null) {
			System.out.println("You are not logged in. Please, sign in or create new account");
			return false;
		} else {
			for (Product p : productList) {
				if (p.id == Integer.parseInt(userInput)) {
					System.out.println(CartManagement.createCartEntry(UsersManagement.loggedInUser.email, p.id));
					return true;
				}
			}
			System.out.println("!!!No valid input!!!");
			return true;
		}
	}

	public static Boolean checkout() {

		System.out.println("Enter your credit card number without spaces and press enter if you confirm purchase");
		String userInput = MainMenuClass.readNextString();

		if (userInput.length() == 16) {
			System.out.println("Thanks a lot for your purchase. Details about order delivery are sent to your email.");
			ArrayList<Integer> productIds = new ArrayList<Integer>();
			for (CartEntry c : CartManagement.getUserCart(UsersManagement.loggedInUser.email)) {
				productIds.add(c.productid);
			}
			OrdersManagement.createOrder(UsersManagement.loggedInUser.email, productIds);
			CartManagement.userCarts.remove(UsersManagement.loggedInUser.email);
			return false;
		} else {
			System.out.println(
					"You entered invalid credit card number. Valid credit card should contain 16 digits. Please, try one more time.");
			return true;
		}
	}
}
