package ECommerceShop;

import java.util.ArrayList;
import java.util.HashMap;

public class OrdersManagement {

	public static HashMap<String, ArrayList<Order>> orderList = new HashMap<>();

	public static void createOrder(String email, ArrayList<Integer> productIds) {
		ArrayList<Order> userOrderList = getUserOrder(email);
		userOrderList.add(new Order(productIds));
		ProductManagement.checkoutPossible = false;
	}

	public static ArrayList<Order> getUserOrder(String email) {
		if (!orderList.containsKey(email)) {
			orderList.put(email, new ArrayList<Order>());
		}
		return orderList.get(email);
	}

	public static void showOrders() {
		System.out.println("");
		System.out.println("*** My Orders ***");
		if (UsersManagement.loggedInUser != null) {
			System.out.println("Order for " + UsersManagement.loggedInUser.email + ":");
			for (Order o : getUserOrder(UsersManagement.loggedInUser.email)) {
				for (Integer pid : o.productids) {
					for (Product p : ProductManagement.getProducts()) {
						if (pid == p.id) {
							System.out.println("Id: " + p.id + " | Name: " + p.productName + " | Category: "
									+ p.categoryName + " | Price: " + p.price);
						}
					}
				}
			}
		} else {
			System.out.println("Please, log in or create new account to see list of your orders");
		}
	}

}
