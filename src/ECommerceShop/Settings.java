package ECommerceShop;

import java.util.Objects;

public class Settings {

	public static void showSettings() {
		if (UsersManagement.isUserLoggedIn()) {
			System.out.println("");
			System.out.println("*** Settings ***");
			System.out.println("1. Change Password");
			System.out.println("2. Change Email");
			System.out.println("3. <-- Back to Main Menu");
			Integer userInput = MainMenuClass.readNextInteger();
			
			if (userInput == 1) {
				changePassword();
			} else if (userInput == 2) {
				changeEmail();
			} else if (userInput == 3) {
				return;
			} else {
				System.out.println("!!!No valid input!!!");
				showSettings();
			}
		} 
		else {
			System.out.println("!!!You need to be logged in to change settings!!!");
		}
		
	}
	
	private static void changePassword() {
		
		System.out.println("Enter your new Password");
		String userInput = MainMenuClass.readNextString();
		
		for(User u : UsersManagement.getUsers()) {
			if(Objects.equals(u.email, UsersManagement.loggedInUser.email)) {
				System.out.println(UsersManagement.changeUserPassword(u.email, userInput));
			}
		}
	}
	
	private static void changeEmail() {
		
		System.out.println("Enter your new Email");
		String userInput = MainMenuClass.readNextString();
		
		for(User u : UsersManagement.getUsers()) {
			if(Objects.equals(u.email, UsersManagement.loggedInUser.email)) {
				System.out.println(UsersManagement.changeUserEmail(u.email, userInput));
			}
		}
	}
	
}
