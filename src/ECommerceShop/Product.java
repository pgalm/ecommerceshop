package ECommerceShop;

public class Product {

	public Integer id;
	public String productName;
	public String categoryName;
	public Double price;
	
	public Product(Integer id, String productName, String categoryName, Double price) {
		super();
		this.id = id;
		this.productName = productName;
		this.categoryName = categoryName;
		this.price = price;
	}
	
}
